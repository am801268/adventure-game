#ifndef POTION_H
#define POTION_H

#include "GameObject.h"

class Potion : public GameObject {
public:
    void use() override;
};

#endif // POTION_H
