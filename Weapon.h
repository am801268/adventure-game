#ifndef WEAPON_H
#define WEAPON_H

#include "GameObject.h"

class Weapon : public GameObject {
public:
    void use() override;
};

#endif // WEAPON_H
