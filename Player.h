#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include "Character.h" 

class Room; 

class Player : public Character {
private:
    std::string name;
    int health;
    Room* location;
public:
    Player(const std::string& name, int health);
    void SetLocation(Room* room);
    Room* GetLocation() const;

    bool IsAlive() const; 

    void Attack(); 

};

#endif 

