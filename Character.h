#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>
#include <vector>
#include "Item.h"

// Forward declaration of Item class
class Item;

class Character {
private:
    std::string name;
    int health;
    std::vector<Item> inventory;

public:
    Character(const std::string& name, int health);
    void TakeDamage(int damage);
    std::string GetName() const;
    int GetHealth() const;
    std::vector<Item>& GetInventory();
};

#endif // CHARACTER_H


