#ifndef COMMAND_INTERPRETER_H
#define COMMAND_INTERPRETER_H

#include <string> 
#include <map>

// Forward declaration of Player class
class Player;

// CommandInterpreter class definition
class CommandInterpreter {
public:
    CommandInterpreter(Player* player);
    void interpretCommand(const std::string& command);

private:
    Player* player_; // Pointer to the player object
};

#endif // COMMAND_INTERPRETER_H
