#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

class GameObject {
public:
    virtual void use() = 0; // Pure virtual function for polymorphic behavior
};

#endif // GAMEOBJECT_H
