#include "CommandInterpreter.h"
#include "Player.h"
#include <iostream>
#include <sstream>

CommandInterpreter::CommandInterpreter(Player* player) : player_(player) {}

void CommandInterpreter::interpretCommand(const std::string& command) {
    // Split the command into words and identify the action and optional parameters
    std::istringstream iss(command);
    std::vector<std::string> words;
    std::string word;
    while (iss >> word) {
        words.push_back(word);
    }

    /
    if (!words.empty()) {
        if (words[0] == "move") {
            // Call the appropriate method on the player object
            player_->move();
        }
        
        else {
            std::cout << "Unknown command: " << command << std::endl;
        }
    }
}
