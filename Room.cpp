#include "Room.h"
#include "Item.h"


Room::Room(const std::string& desc) : description(desc) {}

void Room::AddItem(Item* item) {
    items.push_back(item);
}

void Room::AddExit(const std::string& direction, Room* room) {
    exits[direction] = room;
}

const std::string& Room::GetDescription() const {
    return description;
}

Room* Room::GetExit(const std::string& direction) const {
    auto it = exits.find(direction);
    if (it != exits.end()) {
        return it->second;
    }
    return nullptr;
}

const std::vector<Item*>& Room::GetItems() const {
    return items;
}

std::vector<Item*>& Room::GetItemsMutable() {
    return items;
}

const std::map<std::string, Room*>& Room::GetExits() const {
    return exits;
}

