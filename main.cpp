#include "Area.h"
#include "Player.h"
#include <iostream>
#include "Weapon.h"
#include "Potion.h"
#include "Inventory.h"

int main() {
    // Create an instance of the Area class
    Area gameWorld;

    Inventory inventory;

    Weapon weapon;
    Potion potion;

    inventory.addItem(&weapon);
    inventory.addItem(&potion);

    try {
        // Load the game map from the "game_map.txt" file
        gameWorld.LoadMapFromFile();
    }
    catch (const std::runtime_error& e) {
        std::cerr << e.what() << std::endl;
        return 1; // Exit the program with an error code
    }

    Player player("Alice", 100);


    Room* currentRoom = gameWorld.GetRoom("startRoom");
    if (currentRoom == nullptr) {
        std::cerr << "Error: Starting room not found." << std::endl;
        return 1; // Exit the program with an error code
    }

    player.SetLocation(currentRoom);

    // Game loop
    int choice = 0; // Initialize choice variable here
    std::string direction; // Declare direction variable here
    while (true) {
        std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;

        // Display items in the room
        std::cout << "Items in the room:" << std::endl;
        const std::vector<Item*>& items = player.GetLocation()->GetItems();
        if (!items.empty()) {
            for (const auto& item : items) {
                std::cout << "- " << item->GetName() << ": " << item->GetDescription() << std::endl;
            }
        }
        else {
            std::cout << "The room is empty." << std::endl;
        }

        // Display available exits
        std::cout << "Exits available: ";
        const std::map<std::string, Room*>& exits = player.GetLocation()->GetExits();
        for (const auto& exit : exits) {
            std::cout << exit.first << " ";
        }
        std::cout << "| ";

        while (true) {
            std::cout << "Options:" << std::endl;
            std::cout << "1. Use an item" << std::endl;
            std::cout << "2. Open inventory" << std::endl;
            std::cout << "3. Move to another room" << std::endl;
            std::cout << "4. Quit" << std::endl;
            std::cout << "Enter your choice: ";
            std::cin >> choice;

            switch (choice) {
            case 1:
            
                break;
            case 2:
                // Display the inventory
                inventory.displayInventory();
                break;
            case 3:
                // Move to another room
                std::cout << "Enter the direction (North, South, East, West): ";
                std::cin >> direction; // Initialize direction here

                // Check if the chosen direction is among the available exits
                if (exits.find(direction) != exits.end()) {
                    Room* nextRoom = exits.at(direction);
                    player.SetLocation(nextRoom);
                    std::cout << "You move to the next room." << std::endl;
                    currentRoom = nextRoom;
                }
                else {
                    std::cout << "You can't go that way." << std::endl;
                }
                break;
            case 4:
                std::cout << "Exiting the game." << std::endl;
                return 0;
            default:
                std::cout << "Invalid choice. Try again." << std::endl;
            }
        }
    }


    return 0;
}
