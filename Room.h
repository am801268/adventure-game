#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <vector>
#include <map>
#include "Item.h"

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item*> items;

public:
    Room(const std::string& desc);

    void AddItem(Item* item);
    void AddExit(const std::string& direction, Room* room);

    const std::string& GetDescription() const;
    Room* GetExit(const std::string& direction) const;
    const std::vector<Item*>& GetItems() const; 
    std::vector<Item*>& GetItemsMutable(); 
    const std::map<std::string, Room*>& GetExits() const; 
};


#endif // ROOM_H
