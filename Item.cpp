#include "Item.h"
#include <iostream>

Item::Item(const std::string& name, const std::string& desc) : name(name), description(desc) {}

const std::string& Item::GetName() const {
    return name;
}

const std::string& Item::GetDescription() const {
    return description;
}

void Item::Interact() {
    std::cout << "You interacted with " << name << ".\n";
}
