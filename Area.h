#ifndef AREA_H
#define AREA_H

#include <string>
#include <map>
#include <memory>
#include "Room.h"

class Area {
private:
    std::map<std::string, std::unique_ptr<Room>> rooms;

public:
    Area();
    ~Area();

    void AddRoom(const std::string& name, std::unique_ptr<Room> room);
    Room* GetRoom(const std::string& name);
    void ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction);
    void LoadMapFromFile();
    std::string oppositeDirection(const std::string& direction) const;
};

#endif 
