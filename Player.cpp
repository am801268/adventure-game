#include "Player.h"
#include <iostream>


Player::Player(const std::string& name, int health) : Character(name, health), location(nullptr) {}

// Set the player's location
void Player::SetLocation(Room* newLocation) {
    location = newLocation;
}

// Get the player's location
Room* Player::GetLocation() const {
    return location;
}

// Check if the player is alive
bool Player::IsAlive() const {
    return health > 0;
}

// Attack function (placeholder implementation)
void Player::Attack() {
    
}

//Move the Player 
void Player::Move() {
    std::cout << "Player moves." << std::endl;
}
