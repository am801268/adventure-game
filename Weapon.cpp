#include "Weapon.h"
#include <iostream>

void Weapon::use() {
    std::cout << "Using weapon." << std::endl;
}
