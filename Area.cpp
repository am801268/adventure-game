#include "Area.h"
#include <fstream>
#include <sstream>

Area::Area() {}

Area::~Area() {}

void Area::AddRoom(const std::string& name, std::unique_ptr<Room> room) {
    rooms[name] = std::move(room);
}

Room* Area::GetRoom(const std::string& name) {
    auto it = rooms.find(name);
    return (it != rooms.end()) ? it->second.get() : nullptr;
}

void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
    Room* room1 = GetRoom(room1Name);
    Room* room2 = GetRoom(room2Name);
    if (room1 && room2) {
        room1->AddExit(direction, room2);
        room2->AddExit(oppositeDirection(direction), room1);
    }
}

void Area::LoadMapFromFile() {
    std::ifstream file("game_map.txt");
    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line)) {
            std::istringstream iss(line);
            std::string room1Name, room2Name, direction;
            std::getline(iss, room1Name, '|');
            std::getline(iss, room2Name, '|');
            std::getline(iss, direction);
            AddRoomsFromFile(room1Name, room2Name, direction);
        }
        file.close();
    }
    else {
        throw std::runtime_error("Error: Unable to open file game_map.txt");
    }
}

void Area::AddRoomsFromFile(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
    std::unique_ptr<Room> room1 = std::make_unique<Room>(room1Name);
    std::unique_ptr<Room> room2 = std::make_unique<Room>(room2Name);
    ConnectRooms(room1Name, room2Name, direction);
    AddRoom(room1Name, std::move(room1));
    AddRoom(room2Name, std::move(room2));
}

std::string Area::oppositeDirection(const std::string& direction) const {
    if (direction == "North") return "South";
    else if (direction == "South") return "North";
    else if (direction == "East") return "West";
    else if (direction == "West") return "East";
    // Handle additional directions if needed
    else return ""; // Default case (for invalid direction)
}

