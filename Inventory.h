// Inventory.h
#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>
#include "GameObject.h"

// Inventory class definition
class Inventory {
private:
    std::vector<GameObject*> items;

public:
    void addItem(GameObject* item);
    void displayInventory(); // Add this method declaration
};

#endif // INVENTORY_H
