#include "Character.h"

// Constructor
Character::Character(const std::string& name, int health) : name(name), health(health) {}

// Function to subtract damage from the character's health
void Character::TakeDamage(int damage) {
    health -= damage;
}

std::string Character::GetName() const {
    return name;
}

int Character::GetHealth() const {
    return health;
}

std::vector<Item>& Character::GetInventory() {
    return inventory;
}



