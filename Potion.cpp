#include "Potion.h"
#include <iostream>

void Potion::use() {
    std::cout << "Using potion." << std::endl;
}
